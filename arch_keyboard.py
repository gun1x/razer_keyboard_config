#!/usr/bin/python3
import colorsys
import random

from openrazer.client import DeviceManager
from openrazer.client import constants as razer_constants


def random_color():
    rgb = colorsys.hsv_to_rgb(random.random(), random.random(), 1)
    effect = tuple(map(lambda x: int(256 * x), rgb))
    return effect 

device_manager = DeviceManager()
device_manager.sync_effects = False

device = device_manager.devices[0]
if device.name != "Razer BlackWidow Chroma Tournament Edition":
    device = device_manager.devices[1]

rows, cols = device.fx.advanced.rows, device.fx.advanced.cols

important_color_1 = random_color()
important_color_2 = random_color()
important_color_3 = random_color()

base_color = random_color()
for row in range(rows):
    for col in range(cols):
        device.fx.advanced.matrix[row, col] = base_color
# ins, home, pgup, end, pgdown
device.fx.advanced.matrix[1,15] = important_color_1
device.fx.advanced.matrix[1,16] = important_color_1
device.fx.advanced.matrix[1,17] = important_color_1
device.fx.advanced.matrix[2,16] = important_color_1
device.fx.advanced.matrix[2,17] = important_color_1
# arrows
device.fx.advanced.matrix[4,16] = important_color_1
device.fx.advanced.matrix[5,15] = important_color_1
device.fx.advanced.matrix[5,16] = important_color_1
device.fx.advanced.matrix[5,17] = important_color_1
# tab
device.fx.advanced.matrix[2,1] = important_color_2
# ctrl, alt, del
device.fx.advanced.matrix[5,1] = important_color_2
device.fx.advanced.matrix[5,3] = important_color_2
device.fx.advanced.matrix[2,15] = important_color_2
# arch
device.fx.advanced.matrix[3,2] = important_color_3
device.fx.advanced.matrix[2,5] = important_color_3
device.fx.advanced.matrix[4,5] = important_color_3
device.fx.advanced.matrix[3,7] = important_color_3

device.fx.advanced.draw()

