#!/usr/bin/python3
import colorsys
import random

from openrazer.client import DeviceManager
from openrazer.client import constants as razer_constants

# Create a DeviceManager. This is used to get specific devices
device_manager = DeviceManager()


#print("Found {} Razer devices".format(len(device_manager.devices)))
#print()

# Disable daemon effect syncing.
# Without this, the daemon will try to set the lighting effect to every device.
device_manager.sync_effects = False

# List of effect I've chosen to make an example for
effects = [
    'spectrum',
    'starlight',
    'archkeyboard',
]



def random_color():
    rgb = colorsys.hsv_to_rgb(random.uniform(0, 1), random.uniform(0.5, 1), 1)
    return tuple(map(lambda x: int(256 * x), rgb))

device = device_manager.devices[0]
if device.name != "Razer BlackWidow Chroma Tournament Edition":
    device = device_manager.devices[1]

effect = random.choice(effects)
print("Keyboard mode activated: {}".format(effect))
    
# Ad an example for each effect
if effect == 'breath_random':
    device.fx.breath_random()

elif effect == 'breath_single':
    color = random_color()
    device.fx.breath_single(color[0], color[1], color[2])

elif effect == 'breath_dual':
    color = random_color()
    color2 = random_color()
    device.fx.breath_dual(color[0], color[1], color[2],
                          color2[0], color2[1], color2[2])

elif effect == 'breath_triple':
    color = random_color()
    color2 = random_color()
    color3 = random_color()
    device.fx.breath_triple(color[0], color[1], color[2],
                            color2[0], color2[1], color2[2],
                            color3[0], color3[1], color3[2])

elif effect == 'reactive':
    color = random_color()
    device.fx.reactive(color[0], color[1], color[2], razer_constants.REACTIVE_2000MS)

elif effect == 'spectrum':
    device.fx.spectrum()

elif effect == 'ripple':
    device.fx.ripple_random()

elif effect == 'static':
    color = random_color()
    device.fx.static(*color)

elif effect == 'wave':
    directions = [razer_constants.WAVE_LEFT, razer_constants.WAVE_RIGHT]
    device.fx.wave(random.choice(directions))

elif effect == "starlight":
    rows, cols = device.fx.advanced.rows, device.fx.advanced.cols
    for row in range(rows):
        for col in range(cols):
            device.fx.advanced.matrix[row, col] = random_color()
        device.fx.advanced.draw()

elif effect == "archkeyboard":
    rows, cols = device.fx.advanced.rows, device.fx.advanced.cols

    important_color_1 = random_color()
    important_color_2 = random_color()
    important_color_3 = random_color()

    base_color = random_color()
    for row in range(rows):
        for col in range(cols):
            device.fx.advanced.matrix[row, col] = base_color
    # ins, home, pgup, end, pgdown
    device.fx.advanced.matrix[1,15] = important_color_1
    device.fx.advanced.matrix[1,16] = important_color_1
    device.fx.advanced.matrix[1,17] = important_color_1
    device.fx.advanced.matrix[2,16] = important_color_1
    device.fx.advanced.matrix[2,17] = important_color_1
    # arrows
    device.fx.advanced.matrix[4,16] = important_color_1
    device.fx.advanced.matrix[5,15] = important_color_1
    device.fx.advanced.matrix[5,16] = important_color_1
    device.fx.advanced.matrix[5,17] = important_color_1
    # tab
    device.fx.advanced.matrix[2,1] = important_color_2
    # ctrl, alt, del
    device.fx.advanced.matrix[5,1] = important_color_2
    device.fx.advanced.matrix[5,3] = important_color_2
    device.fx.advanced.matrix[2,15] = important_color_2
    # arch
    device.fx.advanced.matrix[3,2] = important_color_3
    device.fx.advanced.matrix[2,5] = important_color_3
    device.fx.advanced.matrix[4,5] = important_color_3
    device.fx.advanced.matrix[3,7] = important_color_3

    device.fx.advanced.draw()

