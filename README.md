# Razer Keyboard Colors

This is the keyboard config I use for my Razer Keyboard.

Be sure you get https://github.com/openrazer/openrazer before using this.

* `advanced_effect.py` - random effects
* `arch_keyboard.py` - ARCH written on the keyboard, for ArchLinux users

These can be extended so that keyboard colors change depending on what you do on the system.